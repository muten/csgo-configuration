#! /bin/sh

mycp_etc()
{
    directory=`dirname $1`

    if [ ! -d $directory ]
    then mkdir -pv $directory
    fi

    cp -v /$1 $1
}

mycp_steamdir()
{
    directory=steamdir/`dirname $1`

    if [ ! -d $directory ]
    then mkdir -pv $directory
    fi

    cp -v /home/admin_cs/csgo/$1 steamdir/$1
}

mycp_etc etc/csgo-server-launcher/csgo-server-launcher.conf
mycp_etc etc/init.d/csgo-server-launcher

mycp_steamdir csgo/botprofile.db

mycp_steamdir csgo/addons/sourcemod/configs/admin_groups.cfg
mycp_steamdir csgo/addons/sourcemod/configs/admin_levels.cfg
mycp_steamdir csgo/addons/sourcemod/configs/adminmenu_cfgs.txt
mycp_steamdir csgo/addons/sourcemod/configs/adminmenu_custom.txt
mycp_steamdir csgo/addons/sourcemod/configs/adminmenu_grouping.txt
mycp_steamdir csgo/addons/sourcemod/configs/adminmenu_sorting.txt
mycp_steamdir csgo/addons/sourcemod/configs/admin_overrides.cfg

mycp_steamdir csgo/cfg/blank.cfg
mycp_steamdir csgo/cfg/bots_expert.cfg
mycp_steamdir csgo/cfg/bots_hard.cfg
mycp_steamdir csgo/cfg/bots_medium.cfg
mycp_steamdir csgo/cfg/esl5on5-20150521.cfg
mycp_steamdir csgo/cfg/esl5on5.cfg
mycp_steamdir csgo/cfg/gamemode_armsrace.cfg
mycp_steamdir csgo/cfg/gamemode_casual.cfg
mycp_steamdir csgo/cfg/gamemode_competitive.cfg
mycp_steamdir csgo/cfg/gamemode_competitive_default.cfg
mycp_steamdir csgo/cfg/gamemode_competitive_eagles.cfg
mycp_steamdir csgo/cfg/gamemode_cooperative.cfg
mycp_steamdir csgo/cfg/gamemode_custom.cfg
mycp_steamdir csgo/cfg/gamemode_deathmatch.cfg
mycp_steamdir csgo/cfg/gamemode_demolition.cfg
mycp_steamdir csgo/cfg/gamemode_training.cfg
mycp_steamdir csgo/cfg/nobots.cfg
mycp_steamdir csgo/cfg/server.cfg

